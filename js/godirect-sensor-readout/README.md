# Go Direct Sensor Readout


## License

[BSD 3-Clause License](../../LICENSE)

Vernier products are designed for educational use. Our products are not designed nor are they recommended for any industrial, medical, or commercial process such as life support, patient diagnosis, control of a manufacturing process, or industrial testing of any kind.

