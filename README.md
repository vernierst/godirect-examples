# GoDirect Library Examples

Official examples for [Vernier Go Direct® Sensors](https://www.vernier.com/products/sensors/go-direct-sensors) in both Python and JavaScript

## Examples

| Name         | Description                                                      | Python | JavaScript |
|--------------|------------------------------------------------------------------|--------|------------|
| Go Direct Sensor Readout | Get default enabled sensor(s) and log value changes |        | [Go Direct Sensor Readout](https://bitbucket.org/vernierst/godirect-examples/src/master/js/godirect-sensor-readout) |
| Go Direct Explorer | Explore Go Direct devices and view live readouts from multiple sensors | [Go Direct Explorer](https://bitbucket.org/vernierst/godirect-examples/src/master/python/explorer.py) | [Go Direct Explorer](https://bitbucket.org/vernierst/godirect-examples/src/master/js/godirect-explorer/) |
| Go Direct Plotting | Get default enabled sensor(s) and plot 10 measurements | [Go Direct Plotting](https://bitbucket.org/vernierst/godirect-examples/src/master/python/plotting.py) |       |


## License

[BSD 3-Clause License](./LICENSE)

Vernier products are designed for educational use. Our products are not designed nor are they recommended for any industrial, medical, or commercial process such as life support, patient diagnosis, control of a manufacturing process, or industrial testing of any kind.

