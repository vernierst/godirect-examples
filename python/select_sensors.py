
# -*- coding: utf-8 -*-

""" This example automatically connects to a device if it is the
only one connected. If there is more than one it will prompt for
which GoDirect device to connect to. Once opened it prompts for 
the sensors and data rate collect at. 

Installation of the Python module:
# pip install godirect
"""

from gdx import gdx
gdx = gdx()

gdx.open()
gdx.select_sensors()
gdx.start()
sensor_description = gdx.info()
if sensor_description != None:
    print(sensor_description)
for i in range(0,10):
    sensors = gdx.read()
    if sensors != None:
        for sensor in sensors:
            print("{:.2f}".format(sensor.value)+", ", end='')
        print()
gdx.stop()
gdx.close()

