# -*- coding: utf-8 -*-

""" This example shows how to:
* Display a list of available GoDirect devices
* Connect to one or more devices
* List the sensors on the device(s)
* Start collecting data from one or more sensors at a given data rate
* Graph the data

Installation of the Python modules:
# pip install matplotlib
# pip install drawnow
# pip install godirect
"""

from godirect import GoDirect
from plot import GoDirectPlot

import logging
logging.basicConfig()
#logging.getLogger('godirect').setLevel(logging.DEBUG)
#logging.getLogger('pygatt').setLevel(logging.DEBUG)

# Instantiate the python module
godirect = GoDirect(use_ble=True, use_usb=True)
print("GoDirect v"+str(godirect.get_version()))

# List the available GoDirect devices
devices = godirect.list_devices()
print("Found GoDirect devices:")
i=1
selected_devices = []
for device in devices:
    print(str(i)+": "+str(device))
    i += 1

# Create an array of selected GoDirectDevice objects
print("Select devices separated by spaces:", end=' ')
for d in input().split(' '):
    selected_devices.append(devices[int(d)-1])

# Open the selected devices and select sensors
enabled_sensors = []
for device in selected_devices:
    if not device.open():
        print("Failed to open device: "+str(device))
        exit(1)

    # List the available sensors with their period ranges on the device
    sensors = device.list_sensors()
    print("Listing sensors for "+str(device))
    for i in sensors:
        c = sensors[i]
        print(str(c)+"  Period "+str(c.min_measurement_period)+" < typical: "+str(c.typ_measurement_period)+" < "+str(c.max_measurement_period))

    print("Select sensors separated by spaces:", end=' ')
    device.enable_sensors(input().split(' '))
    # Create an array of GoDirectSensor objects to collect from
    enabled_sensors += device.get_enabled_sensors()

# Input the collection period
print("Select period (ms):", end=' ')
period = int(input())

# Input the number of samples to collect
print("Number of samples:", end=' ')
samples = int(input())

# Create a plot for the data
plot = GoDirectPlot(enabled_sensors, period=period)

# Start measurements
for device in selected_devices:
    print("Starting "+str(device))
    if not device.start(period=period):
        print("Failed to start device")

# Read data
for i in range(0,samples):
    # Fetch the latest measurements
    for device in selected_devices:
        if not device.read():
            print("Failed to read device "+str(device))
    # Print out the raw measurements to the console
    for sensor in enabled_sensors:
        print(sensor.sensor_description+": "+str(sensor.value))
    print("----------")
    # Update the plot
    plot.draw()

# Stop measurements and close devices
for device in selected_devices:
    device.stop()
    device.close()

godirect.quit()
# Pause the graph for 10 seconds before it closes
try:
    plot.pause(10)
except:
    pass
plot.close()

