import matplotlib.pyplot as plt
from drawnow import drawnow, figure
import numpy as np

class GoDirectPlot:
    """ Wrapper for the pyplot plot animated with drawnow.
    """

    def __init__(self, sensors=[], period=1):
        """ Create a GoDirectPlot object and instantiate the pyplot plot.

        Args:
            sensors: a list of Sensor objects to graph, the data will be accessed
                directly from these objects with each draw() command
            period: the data collection period in ms, used to keep track of the elapsed
                time for the plots. We do not use the actual timestamps from the sensors
                but rather fake the passing of ms with each draw() command.
        """

        self.ticks = 0
        self.x = []
        self.sensors = sensors
        self.period = period
        self.data = np.array([])

        plt.autoscale(enable=True, axis='both', tight=None)
        plt.ion()

    def _make_fig(self):
        for sensor in self.sensors:
            plt.plot(self.x,sensor.values, label=sensor.sensor_description)
        plt.xlabel("Time (s)")
        legend = plt.legend(loc='upper right', shadow=True, fontsize='small')

    def draw(self):
        """ Call this once after each sensor read() success to update the plot
        """
        self.x.append(self.ticks)
        self.ticks += self.period / 1000
        drawnow(self._make_fig, stop_on_close=True)

    def pause(self, duration):
        """ Pauses the plot for a specified amount of time before the graph window is closed.

        Args:
            duration (int): seconds to leave the plot open
        """
        plt.pause(duration)

    def close(self):
        """ Close the plot window
        """
        plt.close()
