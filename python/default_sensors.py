# -*- coding: utf-8 -*-

""" This example automatically connects to a device if it is the
only one connected. If there is more than one it will prompt for
which GoDirect device to connect to. Once opened it enables the 
default sensors for the device and prompts for the data rate to 
collect at. Once entered it starts reading measurements.

Installation of the Python module:
# pip install godirect
"""

from gdx import gdx
gdx = gdx()

gdx.open()
print(gdx.selected_device._name)
gdx.start()
for i in range(0,10):
    sensors = gdx.read()
    if sensors != None:
        for sensor in sensors:
            print(sensor.sensor_description+": {:.2f}".format(sensor.value))
gdx.stop()
gdx.close()



