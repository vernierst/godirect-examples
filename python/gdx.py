from godirect import GoDirect


import logging
logging.basicConfig()
#logging.getLogger('godirect').setLevel(logging.DEBUG)
#logging.getLogger('pygatt').setLevel(logging.DEBUG)

class gdx:

    def __init__(self):
        self.selected_sensors = []
        self.selected_device = None
        self.godirect = GoDirect(use_ble=True,use_usb=True)

    def select_device(self, found_devices=None):
        if found_devices == None:
            found_devices = self.godirect.list_devices()
        number_found_devices = len(found_devices)
        if number_found_devices >= 1: 
            print("Found " +str(number_found_devices) + " devices:")
            i=1
            for d in found_devices:
                print(str(i)+": "+str(d))
                i += 1

            x = input("Select one device:")
            selected = int(x)
            if selected < number_found_devices:
                 return found_devices[selected-1]
            else:
                print("Selection was invalid")
        else:
            print("No Go Direct Devices Found")
        return None

    def open(self):
        found_devices = self.godirect.list_devices()
        number_found_devices = len(found_devices)
        self.selected_device = None

        if number_found_devices > 1: 
            self.selected_device = self.select_device(found_devices)
        elif number_found_devices == 1: 
            self.selected_device = found_devices[0]

        if self.selected_device != None:
            self.selected_device.open()
        else:
            print("No Go Direct Device Opened")

    def select_sensors(self):
        self.selected_sensors = []

        if self.selected_device == None:
            return 

        sensors = self.selected_device.list_sensors()

        for i in sensors:
            c = sensors[i]
            print(str(c))

        print("Select sensors separated by spaces:", end=' ')
        for s in input().split(' '):
            self.selected_sensors.append(int(s))

    def start(self):
        if self.selected_device == None:
            return 

        print("Select period (ms):", end=' ')
        period = int(input())

        self.selected_device.enable_sensors(sensors=self.selected_sensors)
        self.selected_device.start(period=period)

    def info(self):
        if self.selected_device == None:
            return 
        device = self.selected_device
        sensors = device.get_enabled_sensors()
        sensor_description = []
        for sensor in sensors:
            sensor_description.append(sensor.sensor_description)
        return sensor_description

    def read(self): 
        if self.selected_device == None:
            return None
        if self.selected_device.read():
            return self.selected_device.get_enabled_sensors()
        else:
            return None

    def stop(self):
        if self.selected_device == None:
            return 
        self.selected_device.stop()

    def close(self):
        if self.selected_device == None:
            return 
        self.selected_device.close()

